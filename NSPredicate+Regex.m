//
//  NSPredicate+Regex.m
//  Sevensdog
//
//  Created by SBU on 2016/9/15.
//  Copyright © 2016年 SU PO-YU. All rights reserved.
//

#import "NSPredicate+Regex.h"

@implementation NSPredicate (Regex)

+ (BOOL)validateRagularExpression:(NSString *)strDestination
                     byExpression:(NSString *)strExpression
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", strExpression];
    
    return [predicate evaluateWithObject:strDestination];
}

+ (BOOL)validateEmail:(NSString *)email
{
    NSString *regex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,5}";
    
    return [NSPredicate validateRagularExpression:email byExpression:regex];
}

+ (BOOL)validatePhoneNumber:(NSString *)number
                phoneLength:(NSInteger)length
{
    NSString *regex = [NSString stringWithFormat:@"[0-9]{%ld}", length];

    return [NSPredicate validateRagularExpression:number byExpression:regex];;
}
@end
