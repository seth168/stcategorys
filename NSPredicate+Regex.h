//
//  NSPredicate+Regex.h
//  Sevensdog
//
//  Created by SBU on 2016/9/15.
//  Copyright © 2016年 SU PO-YU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSPredicate (Regex)

/*
 * @brief 驗證表達式是否正確。
 * @param strDestination 驗證內容。
 * @param strExpression 正則表達式。
 * @return 內容正確。
 */
+ (BOOL)validateRagularExpression:(NSString *)strDestination
                     byExpression:(NSString *)strExpression;

/*
 * @brief 驗證電子信箱。
 * @param email 信箱地址。
 * @return 正確電子信箱。
 */
+ (BOOL)validateEmail:(NSString *)email;

/*
 * @brief 驗證手機號碼。
 * @param number 手機號碼。
 * @param length 手機號碼長度。
 * @return 正確手機號碼。
 */
+ (BOOL)validatePhoneNumber:(NSString *)number
                phoneLength:(NSInteger)length;

@end
